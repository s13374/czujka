package czujka;

import java.util.ArrayList;
import java.util.List;


public class Alarm {

	private List<AlarmListener> listeners = new ArrayList<AlarmListener>();
	private String pin = "1111";
	
	public void addListener(AlarmListener listener){
		listeners.add(listener);
	}
	
	public void removeListener(AlarmListener listener) {
		listeners.remove(listener);
	}

	public void enterPin(String pin) {
		if(this.pin.equals(pin)) {
			correctEnteredPin();
		} else wrongEnteredPin();	
	}
	
	private void wrongEnteredPin() {
		
		EnteredPinEvent event = new EnteredPinEvent();
		event.setAlarm(this);
		event.getAlarm();
	
		for(AlarmListener tmp: listeners) {
			tmp.alarmTurnedOn(event);
		}	
	}
	
	private void correctEnteredPin() {
		
		EnteredPinEvent event = new EnteredPinEvent();
		event.setAlarm(this);
		event.getAlarm();
		
		for(AlarmListener tmp: listeners) {
			tmp.alarmTurnedOff(event);
		}
	 }
}