package czujka;

public class AlarmApp {

	public static void main(String[] args) {
		
		Alarm alarm = new Alarm();
		AlarmListener psy = new Dogs();
		AlarmListener kraty = new Bars();
			
		alarm.addListener(psy);
		alarm.addListener(kraty);
		alarm.removeListener(psy);
		
		alarm.enterPin("1112");
	}

}