package czujka;

public class Bars implements AlarmListener {
	
	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Alarm! Opuszczam kraty!");
	};
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Opuszczanie krat nieaktywne.");
	};
}